e3-rflps
======
ESS Site-specific EPICS IOC Application : rflps

## Installation

Clone repository:

```bash
git clone https://bitbucket.org/europeanspallationsource/e3-rflps
cd e3-rflps/
```
Compile the IOC
```bash
make build
sudo make install
```
Source the E3 enviroment
```bash
source /epics/base-3.15.6/require/3.0.5/bin/setE3Env.bash
iocsh.bash cmds//.....
```
