require iocStats,ae5d083
require recsync,1.3.0
require autosave,5.9.0
require s7plc,1.2.0


epicsEnvSet("PREFIX", "TS2-011RFC")
epicsEnvSet("PLCIP", "10.4.3.175")


iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(PREFIX)")
iocshLoad("$(recsync_DIR)/recsync.iocsh",  "IOCNAME=$(PREFIX)")


var s7plcDebug 0

iocshLoad("$(rflps_DIR)/rflps.iocsh", "SUB_NAME=CPU,PLC_IP=$(PLCIP),PLC_PORT=3000,INSIZE=8,  OUTSIZE=4,PREFIX=$(PREFIX)")
iocshLoad("$(rflps_DIR)/rflps.iocsh", "SUB_NAME=AF, PLC_IP=$(PLCIP),PLC_PORT=3001,INSIZE=660,OUTSIZE=390,PREFIX=$(PREFIX)")
iocshLoad("$(rflps_DIR)/rflps.iocsh", "SUB_NAME=DIO,PLC_IP=$(PLCIP),PLC_PORT=3002,INSIZE=336,OUTSIZE=56,PREFIX=$(PREFIX)")
iocshLoad("$(rflps_DIR)/rflps.iocsh", "SUB_NAME=PSU,PLC_IP=$(PLCIP),PLC_PORT=3003,INSIZE=378,OUTSIZE=218,PREFIX=$(PREFIX)")


iocInit()



