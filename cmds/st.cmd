#

require s7plc,1.4.0p


epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")


epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=BENCH-0XXRFC)")
epicsEnvSet("PLCIP", "$(RFLPS_IP=10.4.3.66)")

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "660")
epicsEnvSet("OUTSIZEAF", "390")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "336")
epicsEnvSet("OUTSIZEDIO", "56")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "378")
epicsEnvSet("OUTSIZEPSU", "218")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(TOP)/rflps-loc/rflpsApp/Db/")

dbLoadTemplate("$(TOP)/template/rflpsCPU.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(TOP)/template/rflpsAF.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(TOP)/template/rflpsDIO.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(TOP)/template/rflpsPSU.substitutions", "PREFIX=$(PREFIX)")

# After installation.

# dbLoadRecords("rflpsCPU.db", "PREFIX=$(PREFIX)")
# dbLoadRecords("rflpsAF.db", "PREFIX=$(PREFIX)")
# dbLoadRecords("rflpsDIO", "PREFIX=$(PREFIX)")
# dbLoadRecords("rflpsPSU", "PREFIX=$(PREFIX)")



iocInit


dbl > "$(TOP)/$(PREFIX)_PVs.list"

